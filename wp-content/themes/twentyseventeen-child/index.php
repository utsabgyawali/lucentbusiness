<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<section class="slider"> Section for slider </section>
<!-- about section start-->
<section id="about" class="section-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 heding-wrapper text-center scale-less animate-in animated full-visible">
						<span>CREATIVE WEB TEMPLATE</span>
						<h2>About One Solution</h2>
						<span class="spliter"></span>
					</div>
				</div>
				<div class="row">
					<div class="col-1">
						<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere convallis.Cras porta dolor ut velit imperdiet, quis sodales tellus facilisis. Vestibulum magna turpis, tincidun blandit semper lorem. </p>
					</div>
				</div>
			</div>
			<div class="view-all pt20 text-center">
				<a class="btn default p10-50 scroll" href="#Contact">CONTACT US</a>
				<a class="btn light p10-50 border-1 scroll" href="#portfolio">SEE OUR WORK</a>
			</div>
		</section>
<?php

get_footer();