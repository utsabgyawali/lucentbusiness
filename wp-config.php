<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'trialDb' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '6ExSlcxM]rJr{.^raM%djU A(6b$^OF>r`ej]^Js#r9K1`K`&{4 z5W*wXcCA+0n' );
define( 'SECURE_AUTH_KEY',  '.WQ(mpYN<N|gw8m7,qEF@$I>WHuwQ.JUWMzdkM#7#Q**Oc[6L;-;<u}B:eusQFCS' );
define( 'LOGGED_IN_KEY',    '*/6qCHc#HW^[:T(*W6z~tD[;WFp_079%7x>J9ftZb.l.Uam|nf~koa2&Yw5$-,{w' );
define( 'NONCE_KEY',        'T>3.4?3gBl$8r.d.Y5djQSh;bP|I-rCROO.2Bh9U3h:M<]_$XaZeLTJ:|x,1}lKp' );
define( 'AUTH_SALT',        'C2e88tVqfg&fVJdd.A8&X0_DbPomBg=w<43IQ[T~ KZ}^_:(B >u}dyZ_>p+R_-p' );
define( 'SECURE_AUTH_SALT', 'n/G0QC9neE&ju@_M$4=k^eN)r3~du#!/[=y]l=a !9qve|J$lHpg5fN@c]2h}{P:' );
define( 'LOGGED_IN_SALT',   'kXX*Wh}/n[sDg/7eO+5F964_UYI=l(<<oM ;/SjPnU#2cAH) moo4eps({|Ss})=' );
define( 'NONCE_SALT',       '+F^3sSJIF`:l3.lMiT);dEXaFUY5KL$#S~abKl;sU 606(DRAnPu@4aDu-n`c<2s' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
